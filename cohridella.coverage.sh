#!/bin/bash
#
#SBATCH --job-name=cohridella.coverage

#SBATCH --output=cohridella.coverage_output

# 

#SBATCH -c 30 

# 

#SBATCH --time=96:00:00 

# 

#SBATCH --mem=10G 

# 

#SBATCH --mail-user=luisa.scolari@ist.ac.at 

#SBATCH --mail-type=ALL 

# 

#SBATCH --no-requeue 

# 

#SBATCH --export=NONE 

unset SLURM_EXPORT_ENV 

# 

#Set the number of threads to the SLURM internal variable 

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 

#

#download the module
module load soap


#run SOAPcov (Version: 2.7.7)
srun soap.coverage -sam -cvg -i Female.sam -onlyuniq -p 50 -refsingle Cameraria_ohridella_v1_scaffolds.fasta -o Female.soapcov

srun soap.coverage -sam -cvg -i Male.sam -onlyuniq -p 50 -refsingle Cameraria_ohridella_v1_scaffolds.fasta -o Male.soapcov
