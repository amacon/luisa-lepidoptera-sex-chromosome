#!/bin/bash 

# 

#SBATCH --job-name=bmori.index 

#SBATCH --output=bmori.index _output 

# 

#SBATCH -c 30 

# 

#SBATCH --time=96:00:00 

# 

#SBATCH --mem=10G 

# 

#SBATCH --mail-user=luisa.scolari@ist.ac.at 

#SBATCH --mail-type=ALL 

# 

#SBATCH --no-requeue 

# 

#SBATCH --export=NONE 

unset SLURM_EXPORT_ENV 

# 

#Set the number of threads to the SLURM internal variable 

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK 

# 

 

#DOWNLOAD THE PROGRAM TO MAP 

module load  bowtie2/2.3.4.1 

 

#RUN BOWTIE2 

srun bowtie2-build GCF_014905235.1_Bmori_2016v1.0_genomic.fna BmoriGenome 
